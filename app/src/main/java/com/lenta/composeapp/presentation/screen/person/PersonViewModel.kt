package com.lenta.composeapp.presentation.screen.person

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lenta.composeapp.domain.usecase.GetPersonUseCase
import com.lenta.composeapp.domain.usecase.IGetPersonUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.stateIn

class PersonViewModel(
    savedStateHandle: SavedStateHandle,
    private val personUseCase: IGetPersonUseCase
): ViewModel() {

    private val personId = MutableStateFlow(savedStateHandle.get<Int>("id"))

    val person = personId.filterNotNull().flatMapLatest {
        personUseCase(GetPersonUseCase.Params(personId = it))
    }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), null)
}