package com.lenta.composeapp.domain.usecase

import com.lenta.composeapp.domain.entities.Person
import com.lenta.composeapp.domain.repositories.IPersonRepository
import kotlinx.coroutines.flow.Flow

class GetPersonUseCase(
    private val repository: IPersonRepository,
): IGetPersonUseCase {

    override suspend operator fun invoke(params: Params): Flow<Person> =
        repository.getPerson(params.personId)

    class Params(val personId: Int)
}

interface IGetPersonUseCase {
    suspend operator fun invoke(params: GetPersonUseCase.Params): Flow<Person>
}