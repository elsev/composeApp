package com.lenta.composeapp.data.datasources.remote

import com.lenta.composeapp.data.models.BasePagingResponse
import com.lenta.composeapp.data.models.RawPerson
import com.lenta.composeapp.data.models.RawPersons
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {

    @GET("character")
    suspend fun getPersons(
        @Query("page") page: Int = 1,
    ): RawPersons

    @GET("character")
    suspend fun getPagingPersons(
        @Query("page") page: Int = 1,
    ): BasePagingResponse<RawPerson>

    @GET("character/{personId}")
    suspend fun getPerson(
        @Path("personId") personId: Int
    ): RawPerson
}