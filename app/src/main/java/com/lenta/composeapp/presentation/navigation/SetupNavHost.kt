package com.lenta.composeapp.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.lenta.composeapp.presentation.screen.menu.MenuScreen
import com.lenta.composeapp.presentation.screen.messages.MessagesScreen
import com.lenta.composeapp.presentation.screen.person.PersonScreen
import com.lenta.composeapp.presentation.screen.persons.PersonsScreen
import com.lenta.composeapp.presentation.screen.settings.SettingsScreen

sealed class Screens(val route: String) {
    data object Persons : Screens("persons")
    data object Person : Screens("person")
    data object Menu : Screens("menu")
    data object Messages : Screens("messages")
    data object Settings : Screens("settings")
}

@Composable
fun SetupNavHost(
    navController: NavHostController,
    modifier: Modifier = Modifier
) {
    NavHost(
        navController = navController,
        startDestination = Screens.Persons.route,
        modifier = modifier
    ) {
        composable(Screens.Persons.route) {
            PersonsScreen { personId ->
                navController.navigate(Screens.Person.route + "/$personId")
            }
        }
        composable(
            route = Screens.Person.route + "/{id}",
            arguments = listOf(navArgument("id") { type = NavType.IntType })
        ) { PersonScreen() }
        composable(Screens.Menu.route) { MenuScreen() }
        composable(Screens.Messages.route) { MessagesScreen() }
        composable(Screens.Settings.route) { SettingsScreen() }
    }
}