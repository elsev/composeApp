package com.lenta.composeapp.di

import com.lenta.composeapp.data.repositories.PersonRepository
import com.lenta.composeapp.domain.repositories.IPersonRepository
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module

val dataModule = module {
    singleOf(::PersonRepository) bind IPersonRepository::class
}