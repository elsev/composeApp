package com.lenta.composeapp.data.datasources.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.lenta.composeapp.data.datasources.remote.Api
import com.lenta.composeapp.data.modelmappers.asPerson
import com.lenta.composeapp.data.models.BasePagingResponse
import com.lenta.composeapp.data.models.RawPerson
import com.lenta.composeapp.domain.entities.Person

class PersonsPagingSource(
    private val api: Api
): PagingSource<Int, Person>() {

    override fun getRefreshKey(state: PagingState<Int, Person>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Person> {
        return try {
            val page = params.key ?: 1
            val response = api.getPagingPersons(page)
            val allPages = response.info?.pages ?: 0
            val allCount = response.info?.count ?: 0
            val nextKey = if (LOAD_SIZE * page >= allCount || page >= allPages) null else page + 1
            val prevKey = if (page == 1) null else page - 1
            LoadResult.Page(
                data = response.results.orEmpty().map { it.asPerson() },
                nextKey = nextKey,
                prevKey = prevKey
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    companion object {
        const val LOAD_SIZE = 20
    }
}