package com.lenta.composeapp.di

import com.lenta.composeapp.presentation.screen.persons.PersonsViewModel
import com.lenta.composeapp.presentation.screen.person.PersonViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val appModule = module {
    viewModelOf(::PersonsViewModel)
    viewModelOf(::PersonViewModel)
}

val koinModules = listOf(
    appModule,
    dataModule,
    domainModule,
    networkModule
)