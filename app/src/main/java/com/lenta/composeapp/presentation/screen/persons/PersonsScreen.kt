package com.lenta.composeapp.presentation.screen.persons

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import coil.compose.AsyncImage
import com.lenta.composeapp.R
import com.lenta.composeapp.domain.entities.Person
import org.koin.androidx.compose.koinViewModel

@Composable
fun PersonsScreen(
    viewModel: PersonsViewModel = koinViewModel(),
    onClick: (Int) -> Unit
) {
//    val persons = viewModel.persons.collectAsState().value
    val personsPaging = viewModel.getPersonsPaging().collectAsLazyPagingItems()

    Log.e("TAG", "call")

    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        contentPadding = PaddingValues(16.dp)
    ) {
        items(personsPaging) { person ->
            person?.let {
                PersonItem(
                    person = person,
                    onClick = onClick
                )
            }
        }
    }
}

@Composable
fun PersonItem(
    person: Person,
    onClick: (Int) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(166.dp)
            .background(
                color = colorResource(id = R.color.gray),
                shape = RoundedCornerShape(8.dp)
            )
            .clickable { onClick(person.id) }
    ) {
        AsyncImage(
            modifier = Modifier
                .fillMaxHeight()
                .weight(1f)
                .clip(RoundedCornerShape(topStart = 8.dp, bottomStart = 8.dp)),
            contentScale = ContentScale.Crop,
            model = person.image,
            contentDescription = null
        )
        Column(
            modifier = Modifier
                .padding(8.dp)
                .weight(1f)
        ) {
            Text(
                text = person.name,
                fontSize = 16.sp,
                color = Color.White
            )
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    modifier = Modifier
                        .width(10.dp)
                        .height(10.dp),
                    imageVector = ImageVector.vectorResource(
                        if (person.isAlive()) {
                            R.drawable.ic_circle_green
                        } else {
                            R.drawable.ic_circle_red
                        }
                    ),
                    contentDescription = null
                )
                Text(
                    modifier = Modifier.padding(start = 4.dp),
                    text = person.status,
                    fontSize = 14.sp,
                    color = Color.White
                )
            }
            Text(
                modifier = Modifier.padding(top = 8.dp),
                text = stringResource(R.string.last_known_location),
                fontSize = 14.sp,
                color = colorResource(id = R.color.gray_light)
            )
            Text(
                text = person.location?.name.orEmpty(),
                fontSize = 14.sp,
                color = Color.White
            )
            Text(
                modifier = Modifier.padding(top = 8.dp),
                text = stringResource(R.string.origin),
                fontSize = 14.sp,
                color = colorResource(id = R.color.gray_light)
            )
            Text(
                text = person.origin?.name.orEmpty(),
                fontSize = 14.sp,
                color = Color.White,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}