package com.lenta.composeapp.domain.usecase

import androidx.paging.PagingData
import com.lenta.composeapp.domain.entities.Person
import com.lenta.composeapp.domain.repositories.IPersonRepository
import kotlinx.coroutines.flow.Flow

class GetPersonsPagingUseCase(
    private val repository: IPersonRepository,
): IGetPersonsPagingUseCase {

    override operator fun invoke(): Flow<PagingData<Person>> =
        repository.getPagingPersons()
}

interface IGetPersonsPagingUseCase {

    operator fun invoke(): Flow<PagingData<Person>>
}