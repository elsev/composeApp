package com.lenta.composeapp.di

import com.lenta.composeapp.domain.usecase.GetPersonsUseCase
import com.lenta.composeapp.domain.usecase.IGetPersonsUseCase
import com.lenta.composeapp.domain.usecase.GetPersonUseCase
import com.lenta.composeapp.domain.usecase.IGetPersonUseCase
import com.lenta.composeapp.domain.usecase.GetPersonsPagingUseCase
import com.lenta.composeapp.domain.usecase.IGetPersonsPagingUseCase
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.bind
import org.koin.dsl.module

val domainModule = module {
    factoryOf(::GetPersonsUseCase) bind IGetPersonsUseCase::class
    factoryOf(::GetPersonUseCase) bind IGetPersonUseCase::class
    factoryOf(::GetPersonsPagingUseCase) bind IGetPersonsPagingUseCase::class
}