package com.lenta.composeapp.presentation.screen.person

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.lenta.composeapp.R
import org.koin.androidx.compose.koinViewModel

@Composable
fun PersonScreen(vieModel: PersonViewModel = koinViewModel()) {
    val person = vieModel.person.collectAsState().value

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            modifier = Modifier.padding(top = 16.dp),
            text = person?.name.orEmpty(),
            fontSize = 24.sp,
            color = Color.White
        )
        AsyncImage(
            modifier = Modifier
                .width(260.dp)
                .height(260.dp)
                .padding(top = 16.dp),
            model = person?.image,
            contentDescription = null
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Image(
                modifier = Modifier
                    .width(10.dp)
                    .height(10.dp),
                imageVector = ImageVector.vectorResource(
                    if (person?.isAlive() == true) {
                        R.drawable.ic_circle_green
                    } else {
                        R.drawable.ic_circle_red
                    }
                ),
                contentDescription = null
            )
            Text(
                modifier = Modifier.padding(start = 4.dp),
                text = person?.status.orEmpty(),
                fontSize = 14.sp,
                color = Color.White
            )
        }

        Text(
            modifier = Modifier.padding(top = 8.dp),
            text = stringResource(R.string.gender),
            fontSize = 14.sp,
            color = colorResource(id = R.color.gray_light)
        )
        Text(
            text = person?.gender.orEmpty(),
            fontSize = 14.sp,
            color = Color.White
        )

        Text(
            modifier = Modifier.padding(top = 8.dp),
            text = stringResource(R.string.number_of_episodes),
            fontSize = 14.sp,
            color = colorResource(id = R.color.gray_light)
        )
        Text(
            text = person?.episode?.size?.toString().orEmpty(),
            fontSize = 14.sp,
            color = Color.White
        )

        Text(
            modifier = Modifier.padding(top = 8.dp),
            text = stringResource(R.string.species),
            fontSize = 14.sp,
            color = colorResource(id = R.color.gray_light)
        )
        Text(
            text = person?.species.orEmpty(),
            fontSize = 14.sp,
            color = Color.White
        )

        Text(
            modifier = Modifier.padding(top = 8.dp),
            text = stringResource(R.string.last_known_location),
            fontSize = 14.sp,
            color = colorResource(id = R.color.gray_light)
        )
        Text(
            text = person?.location?.name.orEmpty(),
            fontSize = 14.sp,
            color = Color.White
        )

        Text(
            modifier = Modifier.padding(top = 8.dp),
            text = stringResource(R.string.origin),
            fontSize = 14.sp,
            color = colorResource(id = R.color.gray_light)
        )
        Text(
            text = person?.origin?.name.orEmpty(),
            fontSize = 14.sp,
            color = Color.White,
            overflow = TextOverflow.Ellipsis
        )

        Text(
            modifier = Modifier.padding(top = 8.dp),
            text = stringResource(R.string.was_created),
            fontSize = 14.sp,
            color = colorResource(id = R.color.gray_light)
        )
        Text(
            modifier = Modifier.padding(bottom = 16.dp),
            text = buildString {
                val createdDateWithoutTime = person?.createdDateWithoutTime()
                val createdTime = person?.createdTime()
                if (!createdDateWithoutTime.isNullOrEmpty())
                    append(createdDateWithoutTime)
                if (!createdDateWithoutTime.isNullOrEmpty() && !createdTime.isNullOrEmpty())
                    append(" в ")
                if (!createdTime.isNullOrEmpty())
                    append(createdTime)
            },
            fontSize = 14.sp,
            color = Color.White
        )
    }
}