package com.lenta.composeapp.domain.repositories

import androidx.paging.PagingData
import com.lenta.composeapp.domain.entities.Person
import kotlinx.coroutines.flow.Flow

interface IPersonRepository {

    suspend fun getPersons(): Flow<List<Person>>

    suspend fun getPerson(personId: Int): Flow<Person>

    fun getPagingPersons(): Flow<PagingData<Person>>
}