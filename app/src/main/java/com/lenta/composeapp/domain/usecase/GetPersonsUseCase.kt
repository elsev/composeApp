package com.lenta.composeapp.domain.usecase

import com.lenta.composeapp.domain.entities.Person
import com.lenta.composeapp.domain.repositories.IPersonRepository
import kotlinx.coroutines.flow.Flow

class GetPersonsUseCase(
    private val repository: IPersonRepository,
): IGetPersonsUseCase {

    override suspend operator fun invoke(): Flow<List<Person>> =
        repository.getPersons()
}

interface IGetPersonsUseCase {

    suspend operator fun invoke(): Flow<List<Person>>
}