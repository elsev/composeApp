package com.lenta.composeapp.data.modelmappers

import com.lenta.composeapp.data.models.RawLocation
import com.lenta.composeapp.data.models.RawOrigin
import com.lenta.composeapp.data.models.RawPerson
import com.lenta.composeapp.domain.entities.Location
import com.lenta.composeapp.domain.entities.Origin
import com.lenta.composeapp.domain.entities.Person

fun RawPerson.asPerson() = Person(
    id = id ?: -1,
    name = name.orEmpty(),
    status = status.orEmpty(),
    species = species.orEmpty(),
    type = type.orEmpty(),
    gender = gender.orEmpty(),
    origin = origin?.asOrigin(),
    location = location?.asLocation(),
    image = image.orEmpty(),
    episode = episode.orEmpty(),
    url = url.orEmpty(),
    created = created.orEmpty()
)

fun RawOrigin.asOrigin() = Origin(
    name = name.orEmpty(),
    url = url.orEmpty()
)

fun RawLocation.asLocation() = Location(
    name = name.orEmpty(),
    url = url.orEmpty()
)