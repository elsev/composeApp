package com.lenta.composeapp.presentation.screen.persons

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.lenta.composeapp.domain.entities.Person
import com.lenta.composeapp.domain.usecase.IGetPersonsPagingUseCase
import com.lenta.composeapp.domain.usecase.IGetPersonsUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class PersonsViewModel(
    private val personsUseCase: IGetPersonsUseCase,
    private val personsPagingUseCase: IGetPersonsPagingUseCase
): ViewModel() {

//    private val mPersons = MutableStateFlow<List<Person>>(emptyList())
//    val persons = mPersons.asStateFlow()
//
//    init {
//        viewModelScope.launch {
//            personsUseCase().collectLatest {
//                Log.d("TAG", it.joinToString())
//                mPersons.value = it
//            }
//        }
//    }

    fun getPersonsPaging() = personsPagingUseCase()
}