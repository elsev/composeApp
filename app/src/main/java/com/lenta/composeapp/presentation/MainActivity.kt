package com.lenta.composeapp.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.lenta.composeapp.R
import com.lenta.composeapp.presentation.navigation.Screens
import com.lenta.composeapp.presentation.navigation.SetupNavHost
import com.lenta.composeapp.presentation.screen.menu.MenuScreen
import com.lenta.composeapp.presentation.screen.messages.MessagesScreen
import com.lenta.composeapp.presentation.screen.person.PersonScreen
import com.lenta.composeapp.presentation.screen.persons.PersonsScreen
import com.lenta.composeapp.presentation.screen.settings.SettingsScreen

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            Scaffold(
                topBar = {
                    TopAppBar(
                        title = { Text(text = "Rick And Morty") }
                    )
                },
                bottomBar = {
                    NavigationBar {
                        NavigationBarItem(
                            selected = false,
                            onClick = { navController.navigate(Screens.Persons.route) },
                            icon = { Icon(Icons.Filled.Person, contentDescription = null) }
                        )
                        NavigationBarItem(
                            selected = false,
                            onClick = { navController.navigate(Screens.Menu.route) },
                            icon = { Icon(Icons.Filled.Menu, contentDescription = null) }
                        )
                        NavigationBarItem(
                            selected = false,
                            onClick = { navController.navigate(Screens.Messages.route) },
                            icon = { Icon(Icons.Filled.Email, contentDescription = null) }
                        )
                        NavigationBarItem(
                            selected = false,
                            onClick = { navController.navigate(Screens.Settings.route) },
                            icon = { Icon(Icons.Filled.Settings, contentDescription = null) }
                        )
                    }
                }
            ) { paddingValue ->
                SetupNavHost(
                    navController = navController,
                    modifier = Modifier
                        .padding(paddingValue)
                        .background(color = colorResource(id = R.color.gray_dark))
                )
            }
        }
    }
}