package com.lenta.composeapp.data.repositories

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.lenta.composeapp.data.datasources.paging.PersonsPagingSource
import com.lenta.composeapp.data.datasources.paging.PersonsPagingSource.Companion.LOAD_SIZE
import com.lenta.composeapp.data.datasources.remote.Api
import com.lenta.composeapp.data.modelmappers.asPerson
import com.lenta.composeapp.domain.entities.Person
import com.lenta.composeapp.domain.repositories.IPersonRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

class PersonRepository(
    private val api: Api,
) : IPersonRepository {

    override suspend fun getPerson(personId: Int): Flow<Person> {
        return flowOf(
            api.getPerson(personId)
                .asPerson()
        )
    }

    override suspend fun getPersons(): Flow<List<Person>> {
        return flowOf(
            api.getPersons()
                .results
                ?.map { it.asPerson() }
                .orEmpty()
        )
    }

    override fun getPagingPersons(): Flow<PagingData<Person>> =
        Pager(
            config = PagingConfig(pageSize = LOAD_SIZE),
            pagingSourceFactory = { PersonsPagingSource(api) }
        ).flow
}